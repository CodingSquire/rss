module gitlab.com/CodingSquire/rss

go 1.14

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.7.0
	github.com/mailru/easyjson v0.7.1
	github.com/valyala/fasthttp v1.15.1
)
